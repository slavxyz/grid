<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use App\Grid;

class GridController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $grid = Grid::all();

        return view('dashboard', ['grid' => $grid]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {
        $color = [
            null => 'Choose your color',
            '#ff4444' => 'Red',
            '#CC0000' => 'Dark red',
            '#ffbb33' => 'Yellow',
            '#FF8800' => 'Orange',
            '#00C851' => 'Green',
            '#007E33' => 'Dark Green',
            '#33b5e5' => 'Blue',
            '#0099CC' => 'Dark Blue',
        ];
        $grid = new Grid();
        $id = request('id');
        return View::make('gridForm', ['id' => $id, 'color' => $color, 'grid' => $grid]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
         $id = request('id'); 
        Grid::where('id', '=', $id)
                ->update([
                    'title' => request('title'),
                    'link' => request('link'),
                    'color' => request('color'),
                ]);


        return response()->json([
                    "message" => "Success"
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
