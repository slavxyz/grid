(function () {
    let  url = document.querySelector('form').getAttribute('action'),
            baseUrl = document.querySelector('#baseUrl').value,
            currenUr1 = document.querySelector('#currentId').value,
            minLenght = 4,
            errors = {
                minLen: `${minLenght} characters minimum`,
                requiredError: 'Required field',
                linkError: 'The link is not valid!',
                colorError: 'Please choose your color!;'
            },
            pattern = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/,
            form = document.querySelector('form');
            
    let checkBeforeSend = function(){
        return form.title.value.length > minLenght 
                && pattern.test(form.link.value) 
                && form.color.value !== '';
    };
    
    let disableForm = function(){
        for (let i = 0, id = '', formLength = form.elements.length; i < formLength; i++) {
             if(form.elements[i].type == 'hidden') continue;
             form.elements[i].disabled = true;
         }     
    };
    
    let reset = function () {
        form.reset();
        $('#title-check').addClass('d-none');
        $('#link-check').addClass('d-none');
        $('#color-check').addClass('d-none');
        disableForm();
    };

    form.title.onfocus = function () {
        form.title.onkeyup = function ()
        {
            if(this.value.length === 0){
                $('#title-check').addClass('d-none');
                $('#title').addClass('error').next().text(errors.requiredError);
            }else if(this.value.length < minLenght) {
                $('#title-check').addClass('d-none');
                $('#title').addClass('error').next().text(errors.minLen);
            } else {
                $('#title-check').removeClass('d-none');
                $('#title').removeClass('error').next().empty();
            };
            
        };
    };

    form.link.onfocus = function () {
        form.link.onkeyup = function ()
        {
            if (this.value.length > minLenght) {
                if (pattern.test(this.value)) {
                    $('#link-check').removeClass('d-none');
                    $('#link').removeClass('error').next().empty();
                }else{
                    $('#link').addClass('error').next().text(errors.linkError);         
                }
            } else {
                $('#link-check').addClass('d-none');
                $('#link').addClass('error').next().text(errors.requiredError);
            }
        };
    };

    form.color.onfocus = function () {
        form.color.onchange = function ()
        {
            if (this.value !== '') {
                $('#color-check').removeClass('d-none');
                $('#color').removeClass('error');
            } else {
                $('#color-check').addClass('d-none');
                $('#color').addClass('error');
            }
        };
    };

    window.addEventListener("click", () => {

        if (checkBeforeSend()) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: url,
                data: {id: currenUr1, title: form.title.value, link: form.link.value, color: form.color.value},
                success: function (data) {
                    reset();
                    $('#form-success').removeClass('d-none');
                    setTimeout(function () {
                        window.location.href = baseUrl;
                    }, 3000);

                },
                error: function (xhr, status, error) {
                    var errorMessage = xhr.status + ': ' + xhr.statusText
                    alert(errorMessage);
                }
            });
        }
    });
})();


