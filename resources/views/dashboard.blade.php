@extends('app')
@section('content')
<div id="wrapper mt-0" >
    <div class="container-fluid">
        <div class="row">
            @foreach ($grid as $item)
            @if ($item->title === '' && $item->link === '' && $item->color === '' )
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                <div class="new text-center">
                    <a href="{{ route('grid.create',['id' => $item->id])}}" >+</a>
                </div>
            </div>
            @continue
            @endif
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-12">
                <div class="box text-center" >
                    <a  class="shadow" style='background-color: {{$item->color}}' data-toggle="tooltip" title="{{$item->title}}" href="{{$item->link}}">
                        {!! Str::limit($item->title, 10, '...') !!}
                    </a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@stop

