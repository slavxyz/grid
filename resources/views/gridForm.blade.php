@extends('app')
@section('content')
<div class="container">
    <div class="row">
        <div id="grid-form" class="mt-5 " >
            {!! Form::model($grid, ['action' => 'GridController@store', 'id' => 'form','class' => '']) !!}
            <div class="form-group row d-flex justify-content-center">
                {!! Form::label('title', 'Title:*', ['class' => 'col-lg-1 col-md-1 col-sm-1 col-1 col-form-label col-form-label-sm']) !!}
                <div class="col-lg-4 col-md-6 col-sm-9 col-9">
                    {!! Form::text('title', '', ['id' => 'title', 'class' => 'form-control', 'placeholder' => ""]) !!}
                    <div class="error-message text-danger"></div>
                </div>
                <div class="col-sm-1 col-1">
                    <i  id="title-check" class="fas fa-check align-bottom text-success fa-2x d-none"></i>
                </div>
            </div>
            <div class="form-group row d-flex justify-content-center">
                {!! Form::label('link', 'Link:*', ['class' => 'col-lg-1 col-md-1 col-sm-1 col-1 col-form-label col-form-label-sm']) !!}
                <div class="col-lg-4 col-md-6 col-sm-9 col-9">
                    {!! Form::text('link', '', ['id' => 'link', 'class' => 'form-control', 'placeholder' => "ex: http://google.com"]) !!}
                    <div class="error-message text-danger"></div>
                </div>
                <div class="col-sm-1 col-1">
                    <i  id="link-check" class="fas fa-check align-bottom text-success fa-2x d-none"></i>
                </div>
            </div>
            <div class="form-group row d-flex justify-content-center">
                {!! Form::label('title', 'Color:*', ['class' => 'col-lg-1 col-md-1 col-sm-1 col-1 col-form-label col-form-label-sm']) !!}
                <div class="col-lg-4 col-md-6 col-sm-9 col-9">
                    {!! Form::select('color', $color, null, ['id' => 'color', 'class' => 'form-control']) !!}
                    <div class="error-message text-danger"></div>
                </div>

                <div class="col-sm-1 col-1">
                    <i  id="color-check" class="fas fa-check align-bottom text-success fa-2x d-none"></i>
                </div>
                <input type="hidden" id="baseUrl"  value="{{ url('/') }}">
                <input type="hidden" id="currentId"  value="{{ $id }}">
            </div>
            <div id="form-success" class="text-center d-none bg-success">
                Form sended successfully.Automatically redirect to home page.
            </div>    
            {!! Form::close() !!}
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('js/grid.js') }}"></script>
@endsection